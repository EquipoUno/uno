package modelo;
import controlador.Cuate;
import controlador.CuateBuscaInterface;
import java.util.Collection;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Edgar
 */
@ManagedBean(name = "cuate")
@SessionScoped
public class CuateBean 
{
    private String user;
    private String password; 
    private Cuate cuate;
    private Collection <Cuate> listaCuate=null;

    public Collection<Cuate> getListaCuate() {
        listaCuate=cuates.ListaCuate();
        return listaCuate;
    }
    @ManagedProperty(value="#{buscaCuates}")
    private CuateBuscaInterface cuates;
   
    public CuateBean() {
    }
    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public String pagoCuotas()
    {
        String pagina ="";
        setCuate(getCuates().BuscaCuate(user, password));
        if(getCuate()==null)
        {
            pagina="Intruso";
        }
        else
        {
            if(getCuate().getCuotas()>100)
            {
                pagina="Deudor";
            }
            else
            {
                   
                pagina="ListaCuates";
                  
            }
            
            
        }
        return pagina;
    }


    public Cuate getCuate() {
        return cuate;
    }

 
    public void setCuate(Cuate cuate) {
        this.cuate = cuate;
    }

 
    public CuateBuscaInterface getCuates() {
        return cuates;
    }

    public void setCuates(CuateBuscaInterface cuates) {
        this.cuates = cuates;
    }
    
}
