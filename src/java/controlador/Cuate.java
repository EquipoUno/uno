
package controlador;

/**
 *
 * @author Edgar
 */
public class Cuate 
{
    private String user;
    private String pwd;
    private String apellidos;
    private String nombres;
    private double cuotas;
    
    public Cuate(String u, String p, String ap, String n, double c)
    {
        user=u;
        pwd=p;
        apellidos=ap;
        nombres=n;
        cuotas=c;
        
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public double getCuotas() {
        return cuotas;
    }

    public void setCuotas(double cuotas) {
        this.cuotas = cuotas;
    }
    
    
}
