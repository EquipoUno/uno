
package controlador;

import java.util.Collection;
/**
 *
 * @author Edgar
 */
public interface CuateBuscaInterface 
{
    public Cuate BuscaCuate(String login,String pwd);
    public Collection<Cuate> ListaCuate();
    
}
