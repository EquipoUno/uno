package controlador;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author Edgar
 */
@ManagedBean(name="buscaCuates",eager=true)
@ApplicationScoped
public class MapeaCuates implements CuateBuscaInterface
{
    private Collection <Cuate> lista= new ArrayList<Cuate>();
    private Map<String,Cuate > cuates;
    public MapeaCuates()
    {
        cuates=new HashMap<>();
        System.out.println("CREANDO CUATES");
        addCuate(new Cuate("laaguilar","aguilar","luis armando","aguilar",200.00));
        addCuate(new Cuate("ecirilo","cirilo","edgar","cirilo",70.00));
        addCuate(new Cuate("jahernandez","hernandez","jorge antonio","hernandez",80.00));
        addCuate(new Cuate("ejhernandez","hernandez","erick jonathan","hernandez",40.00));        
    }

    @Override
    public Cuate BuscaCuate(String user, String pwd) 
    {
        Cuate cuate=null;
        System.out.println(user);
        if (user!=null)
        {
            cuate=cuates.get(user.toLowerCase());
            if (cuate!=null)
            {
                if (cuate.getPwd().equals(pwd))
                {
                    System.out.println("ES IGUAL EL PASSWORD");
                    return cuate;
                }
                else
                    return (null);
            }
        }
        return cuate;
    }
    
    private void addCuate(Cuate cuate)
    {
        System.out.println("CUOTAS:"+ cuate.getCuotas());
        cuates.put(cuate.getUser(), cuate);
        lista.add(cuate); 
    }

    @Override
    public Collection<Cuate> ListaCuate() 
    {
        
        return lista;
    }
    
}
